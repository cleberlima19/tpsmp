console.log("Hello World!");
let variavel = -5;
console.log("variável " + variavel + " é do tipo: " + typeof(variavel));

//operadores matemáticos
let numero1 =5;
let numero2= 10;

console.log(""+numero1+""+" "+numero2);

console.log(5*2);

try{
    let idade = 40;

    if(idade == undefined){
        console.log("Idade não foi criada!");   
    }else{
        console.log("Sua idade é: " + idade);
    }
}catch(error){
    console.log("Erro na variável idade");
}

function mostrarIdade(){
    let idade = 42;
    if(idade < 42){
        console.log("Jovem");
    }else{
        console.log("Não muito jovem");
    }
}
mostrarIdade();

function mostrarIdade2(vlidade){
    if(vlidade < 42){
        console.log(vlidade + " - Jovem");
    }else{
        console.log(vlidade + " - Não muito jovem");
    }
}
mostrarIdade2(42);

const mostrarIdade3 = (vlidade) => {
    if(vlidade < 42){
        console.log(vlidade + " - Jovem - versão 3");
    }else{
        console.log(vlidade + " - Não muito jovem - versão 3");
    }
}
mostrarIdade3(42);